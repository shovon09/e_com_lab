<?php $SD = 'http://src.juicyredapple.com.au/'; ?>
<html>
<head>
    <script type="text/javascript" src="<?php echo $SD;?>js/ajaxfileupload-jquery-1.3.2.js"></script>
    <script type="text/javascript" src="<?php echo $SD;?>js/ajaxupload.3.5.js"></script>

    <script type="text/javascript">
        $(function () {
            var btnUpload = $('#photo_btn');
            var uploadStatus = $('#up_load_sts');
            var files = $('#files');
            new AjaxUpload(btnUpload, {
                action:'uploadPhoto.php',
                name:'uploadfile',
                onSubmit:function (file, ext) {
                    if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
                        // extension is not allowed
                        uploadStatus.text('Only JPG, PNG or GIF files are allowed');
                        return false;
                    }
                    uploadStatus.html('<img src="ajax-loader.gif" height="16" width="16">');
                },
                onComplete:function (file, response) {
                    //On completion clear the status
                    uploadStatus.text('Photo Uploaded Sucessfully!');
                    //On completion clear the status
                    files.html('');
                    //Add uploaded file to list
                    if (response === "success") {
                        $('<div></div>').appendTo('#files').html('<img src="<?php echo $SD;?>img/upgrade_profile/webinfopedia_' + file + '" alt="" height="120" width="130" /><br />').addClass('success');
                    } else {
                        $('<div></div>').appendTo('#files').text(file).addClass('error');
                    }
                }
            });

        });
    </script>
</head>
<body>

<div id="photo_btn" class="styleall">

    <input type="button" value="Upload Photo"/>

</div>

<span id="up_load_sts"></span>

<div id="files">
    <div class="success">
    </div>
</div>
</body>
</html>