<?php
/**
 * Created by JetBrains PhpStorm.
 * User: student
 * Date: 3/15/13
 * Time: 3:54 PM
 * To change this template use File | Settings | File Templates.
 */

?>
<html>
<head>

    <script type="text/javascript">
        var counter = 2;
        function addInput(divName)
        {
            {
                counter++;
                var newdiv = document.createElement('div' + (counter));
                newdiv.innerHTML = "</br>"+"Input"+ (counter)+ ":<input type='text' name='levels[]'/>";
                document.getElementById(divName).appendChild(newdiv);
            }
        }
        function removeInput()
        {

            if (counter > 2)
            {
                document.getElementById('dynamicInput').removeChild(document.getElementById('dynamicInput').lastChild)
                --counter;
            }
        }
        function sumInput(){

            var iterator=0;
            var sum=0;
            for(iterator=0;iterator<counter;iterator++){
                sum =sum+parseInt(document.getElementById("dynamicInput").getElementsByTagName("input").item(iterator).value);
            }
            alert("The summation is : " + sum);

        }
    </script>
</head>
<body>
<p>Addition</p>
<div>
    <form name="inputForm">

        <div id="dynamicInput">
            <label>Input1:</label> <input type="text" name="levels[]"/></br>
            <label>Input2:</label> <input type="text" name="levels[]"/>

        </div>

        <input type="button" value="Add" onClick="addInput('dynamicInput');"/> </br>

        <div id="dynamicRemove">
            <input type="button" value="Remove" onClick="removeInput();"/>
        </div>
        <input type="button" value="GO" onclick="sumInput()" />

    </form>
</div>
</body>
</html>
