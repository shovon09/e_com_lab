<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Shovon
 * Date: 3/28/13
 * Time: 10:13 AM
 * To change this template use File | Settings | File Templates.
 */

require_once 'db_config.php';
$perPage=10;
if (isset($_GET["page"])) {
    $page = intval($_GET["page"]);
} else {
    $page = 1;
}
if (isset($_GET["page_threshold"])) {
    $pageThreshold = intval($_GET["page_threshold"]);
} else {
    $pageThreshold= 1;
}

$cal = $perPage * $page;
$starPage = $cal - $perPage;

$query = "SELECT * FROM `books` LIMIT " . $starPage . "," . $perPage;

$resultSet = mysqli_query($connection, $query);
?>
<html>
<head>
    <title>Database Pagination</title>
</head>
<body>


<table border="1">
    <tr>
        <th>ID</th>
        <th>ISBN</th>
        <th>Title</th>
        <th>Author</th>
        <th>Publisher</th>
    </tr>
    <?php
    while ($result = mysqli_fetch_array($resultSet)) {
//        echo $row['FirstName'] . " " . $row['LastName'];
//        echo "<br>";
        echo "<tr>";
        echo "<td>".$result['id']."</td>";
        echo "<td>".$result['ISBN']."</td>";
        echo "<td>".$result['title']."</td>";
        echo "<td>".$result['author']."</td>";
        echo "<td>".$result['publisher']."</td>";
        echo "</tr>";
    }
    ?>


</table>

<?php
if(isset($page))
{
    $result = mysqli_query($connection,"select Count(*) As Total from books");
    $rows = mysqli_num_rows($result);
    if($rows)
    {
        $rs = mysqli_fetch_array($result);
        $total = $rs["Total"];
    }
    $totalPages = ceil($total / $perPage);

    if($pageThreshold <=1 &&$pageThreshold>0 )
    {
        echo "<span id='page_links' style='font-weight:bold;'>Pre</span>";
    }
    else
    {   $k=$pageThreshold-10;
        $j = $page;
        echo "<span><a id='page_a_link' href='index.php?page=$j&page_threshold=$k'>< Pre </a></span>";
    }
    for($i=$pageThreshold; $i <= $pageThreshold+10; $i++)
    {
        if($i<>$page)
        {
            echo "<span><a href='index.php?page=$i&page_threshold=$pageThreshold' id='page_a_link'>"."-> ".$i."</a></span>";
        }
        else
        {
            echo "<span id='page_links' style='font-weight:bold;'>$i</span>";
        }
    }
    if($pageThreshold == $totalPages )
    {
        echo "<span id='page_links' style='font-weight:bold;'>Next ></span>";
    }
    else
    {
        $k=$pageThreshold+10;
        $j = $page;
        echo "<span><a href='index.php?page=$j&page_threshold=$k'id='page_a_link'>Next</a></span>";
    }
}
?>

</body>

</html>