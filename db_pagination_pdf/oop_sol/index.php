<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Shovon
 * Date: 3/28/13
 * Time: 10:13 AM
 * To change this template use File | Settings | File Templates.
 */

include "Pagination.php";

$hostName = "localhost";
$userName = "root";
$password = "";
$dbName = "e_com_lab";

$pagination = new Pagination();
$pagination->installDatabase($hostName, $userName, $password, $dbName);
$pagination->initiatePage();

if (isset($_GET["search"])) {
    if ($_GET["search"] != null || $_GET["search"] != "") {
        $pagination->getSearchResultPage($_GET["search"]);
    } else {
        $pagination->getResultPage();

    }
} else {
    $pagination->getResultPage();

}
?>
<html>
<head>
    <title>Database Pagination</title>
</head>
<body>
<form action="index.php" method="get">
    Filter Result(by title) : <input type="search" name="search"/>
</form>
<table border="1">
    <tr>
        <th>ID</th>
        <th>ISBN</th>
        <th>Title</th>
        <th>Author</th>
        <th>Publisher</th>
    </tr>
    <?php
    $pagination->printResultTable();
    ?>
</table>

<?php
$pagination->makePagination("index.php");
$pagination->closeDatabase();
?>

</body>

</html>