<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Shovon
 * Date: 3/29/13
 * Time: 12:29 AM
 * To change this template use File | Settings | File Templates.
 */
class Pagination
{
    private $perPage;
    private $page;
    private $pageThreshold;
    private $cal;
    private $starPage;
    private $resultSet;
    private $connection;


    public function installDatabase($hostName, $userName, $password, $dbName)
    {
        $this->connection = mysqli_connect($hostName, $userName, $password, $dbName);
    }

    public function initiatePage()
    {
        $this->perPage = 10;
        if (isset($_GET["page"])) {
            $this->page = intval($_GET["page"]);
        } else {
            $this->page = 1;
        }
        if (isset($_GET["page_threshold"])) {
            $this->pageThreshold = intval($_GET["page_threshold"]);
        } else {
            $this->pageThreshold = 1;
        }

        $this->cal = $this->perPage * $this->page;
        $this->starPage = $this->cal - $this->perPage;


    }

    public function  getResultPage()
    {
        $query = "SELECT * FROM `books` LIMIT " . $this->starPage . "," . $this->perPage;

        $this->resultSet = mysqli_query($this->connection, $query);
        return $this->resultSet;
    }
    public function getSearchResultPage($searchIndex){
        $query = "SELECT * FROM `books` WHERE `title` LIKE '%$searchIndex%' LIMIT " . $this->starPage . "," . $this->perPage;

        $this->resultSet = mysqli_query($this->connection, $query);
        return $this->resultSet;
    }
    public function makePagination($basePath)
    {
        if (isset($this->page)) {
            $result = mysqli_query($this->connection, "select Count(*) As Total from books");
            $rows = mysqli_num_rows($result);
            if ($rows) {
                $rs = mysqli_fetch_array($result);
                $total = $rs["Total"];
            }
            $totalPages = ceil($total / $this->perPage);

            if ($this->pageThreshold <= 1 && $this->pageThreshold > 0) {
                echo "<span id='page_links' style='font-weight:bold;'>Pre</span>";
            } else {
                $k = $this->pageThreshold - 10;
                $j = $this->page;
                echo "<span><a id='page_a_link' href='$basePath?page=$j&page_threshold=$k'>< Pre </a></span>";
            }
            for ($i = $this->pageThreshold; $i <= $this->pageThreshold + 10; $i++) {
                if ($i <> $this->page) {
                    echo "<span><a href='$basePath?page=$i&page_threshold=$this->pageThreshold' id='page_a_link'>" . "-> " . $i . "</a></span>";
                } else {
                    echo "<span id='page_links' style='font-weight:bold;'>$i</span>";
                }
            }
            if ($this->pageThreshold == $totalPages) {
                echo "<span id='page_links' style='font-weight:bold;'>Next ></span>";
            } else {
                $k = $this->pageThreshold + 10;
                $j = $this->page;
                echo "<span><a href='$basePath?page=$j&page_threshold=$k'id='page_a_link'>Next</a></span>";
            }
        }
    }

    public function printResultTable()
    {
        while ($result = mysqli_fetch_array($this->resultSet)) {
            echo "<tr>";
            echo "<td>" . $result['id'] . "</td>";
            echo "<td>" . $result['ISBN'] . "</td>";
            echo "<td>" . $result['title'] . "</td>";
            echo "<td>" . $result['author'] . "</td>";
            echo "<td>" . $result['publisher'] . "</td>";
            echo "</tr>";
        }
    }

    public function closeDatabase()
    {
        mysqli_close($this->connection);
    }

    public function searchResult($isbn)
    {

    }


}
