<?php

include "mainPage.php";

$hostName = "localhost";
$userName = "root";
$password = "";
$dbName = "e_com_lab";

$pagination = new Pagination();
$pagination->installDatabase($hostName, $userName, $password, $dbName);
$pagination->initiatePage();


    $pagination->getResultPage();


?>
<html>
<head>
    <title>Database Pagination</title>
</head>
<body>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Address</th>
        <th>pic</th>
    </tr>
    <?php
    $pagination->printResultTable();
    ?>
</table>

<?php
$pagination->makePagination("index.php");
$pagination->closeDatabase();
?>

</body>

</html>