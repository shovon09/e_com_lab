<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rifatul
 * Date: 2/14/13
 * Time: 7:11 AM
 * To change this template use File | Settings | File Templates.
 */

$optionVales = array("A", "B", "C", "D","E","F");
$hiddenValues = array();

if (isset($_GET["option"])) {
    $hiddenValues[] = $_GET["option"];
}

if (isset($_GET["hiddenFi"])) {
    foreach ($_GET["hiddenFi"] as $value) {
        $hiddenValues[] = $value;
    }
}

if (isset($_GET["showButtonClicked"])) {
    if (isset($_GET["hiddenFi"])) {
        foreach ($_GET["hiddenFi"] as $value) {
            echo $value;
        }
    }
}

?>


<html>
<head>
    <body>
    <form action="combo.php" method="get">
        <select name="option">

            <?php
            foreach ($optionVales as $option) {
                if (!in_array($option, $hiddenValues))
                    echo " <option value='" . $option . "'> " . $option . " </option>";
            }
            ?>

        </select>

        <?php

        foreach ($hiddenValues as $hiddenValue) {
            echo "<input name ='hiddenFi[]' type='hidden' value='" . $hiddenValue . "'/>";
        }

        ?>

        <input type="submit" value="submit"/>

    </form>
    <form action="combo.php" method="get">
        <input name="showButtonClicked" type="hidden" value="true"/>
        <?php
        foreach ($hiddenValues as $hiddenValue) {
            echo "<input name ='hiddenFi[]' type='hidden' value='" . $hiddenValue . "'/>";
        }
        ?>
        <input type="submit" value="show"/>
    </form>

    </body>
</head>
</html>