<?php
/**
 * Created by JetBrains PhpStorm.
 * User: rifatul
 * Date: 2/15/13
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */
class ComboClass
{
    private $optionValues;
    private $hiddenValues;


    function __construct()
    {

        $this->hiddenValues = array();
    }

    public function setHiddenValues($hiddenValues)
    {
        $this->hiddenValues[] = $hiddenValues;
    }

    public function getHiddenValues()
    {
        return $this->hiddenValues;
    }

    public function setOptionValues($optionValues)
    {
        $this->optionValues = $optionValues;
    }

    public function getOptionValues()
    {
        return $this->optionValues;
    }

    public function getComboBoxForm()
    {
        foreach ($this->optionValues as $option) {
            if (!in_array($option, $this->hiddenValues)) {
                echo " <option value='" . $option . "'> " . $option . " </option>";
            }
        }
    }

    public function getHiddenInputForm()
    {
        foreach ($this->hiddenValues as $hiddenValue) {
            echo "<input name ='hiddenFi[]' type='hidden' value='" . $hiddenValue . "'/>";
        }
    }

}

//

$combo = new ComboClass();
$combo->setOptionValues(array("A", "B", "C"));

if (isset($_GET["option"])) {
//    $hiddenValues[] = ;
    $combo->setHiddenValues($_GET["option"]);
}

if (isset($_GET["hiddenFi"])) {
    foreach ($_GET["hiddenFi"] as $value) {
//        $hiddenValues[] = $value;
        $combo->setHiddenValues($value);
    }
}

if (isset($_GET["showButtonClicked"])) {
    if (isset($_GET["hiddenFi"])) {
        foreach ($_GET["hiddenFi"] as $value) {
            echo $value;
        }
    }
}

?>

<html>
<head>
    <body>
    <form action="combo_in_oop.php" method="get">
        <select name="option">

            <?php
            $combo->getComboBoxForm();
            ?>

        </select>

        <?php

        $combo->getHiddenInputForm();
        ?>

        <input type="submit" value="submit"/>

    </form>
    <form action="combo_in_oop.php" method="get">
        <input name="showButtonClicked" type="hidden" value="true"/>
        <?php

        $combo->getHiddenInputForm();
        ?>
        <input type="submit" value="show"/>
    </form>

    </body>
</head>
</html>